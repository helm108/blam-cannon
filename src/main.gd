extends Node2D

@onready var turret = $turret
@onready var wave_spawner: WaveSpawner = $WaveSpawner
@onready var gui: CanvasLayer = $GUI

func _ready():
	wave_spawner.wave_started.connect(_wave_started)
	
#	EventBus.wave_start.emit()
	EventBus.wave_end.emit()

func _wave_started(wave_number: int):
	gui.update_wave(wave_number)
