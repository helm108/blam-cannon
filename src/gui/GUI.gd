extends CanvasLayer

@onready var money_value: Label = $Control/HBoxContainer/MarginContainer/Money/MoneyValue
@onready var health_value: Label = $Control/HBoxContainer/MarginContainer2/Health/HealthValue
@onready var wave_value: Label = $Control/HBoxContainer/MarginContainer3/Wave/WaveValue
@onready var message_label: Label = $Control/MarginContainer/CenterContainer/MessageLabel

func _ready():
	PlayerState.money_updated.connect(_update_money)
	PlayerState.health_updated.connect(_update_health)
	
	EventBus.show_message.connect(set_message)
	EventBus.clear_message.connect(clear_message)
	EventBus.queue_messages.connect(queue_messages)
	
	_update_money(PlayerState.MONEY)
	_update_health(PlayerState.HEALTH, PlayerState.MAX_HEALTH)
	
func set_message(value: String, duration: int) -> void:
	message_label.text = value
	message_label.visible = true
	await get_tree().create_timer(duration).timeout
	clear_message()
	
func queue_messages(values: Array[String], duration: int) -> void:
	message_label.visible = true
	for value in values:
		message_label.text = value
		await get_tree().create_timer(duration).timeout
	clear_message()

func clear_message() -> void:
	message_label.text = ''
	message_label.visible = false

func _update_money(value: int) -> void:
	money_value.text = "%s" % value

func _update_health(current_health: int, max_health: int) -> void:
	health_value.text = "%s/%s" % [current_health, max_health]

func update_wave(wave_number: int):
	wave_value.text = "%s" % wave_number
