extends PanelContainer
class_name UpgradePanel

@onready var name_label: Label = $NameLabel
@onready var cost_label: Label = $CostLabel
@onready var texture_rect: TextureRect = $MarginContainer/TextureRect

func set_upgrade_definition(upgrade_definition: UpgradeDefinition) -> void:
	tooltip_text = upgrade_definition.description
	name_label.text = upgrade_definition.name
	cost_label.text = str(upgrade_definition.get_cost())
	
	if upgrade_definition.is_purchased():
		cost_label.add_theme_color_override("font_color", Color.GRAY)
		name_label.add_theme_color_override("font_color", Color.GRAY)
		texture_rect.modulate = Color.hex(777777)
