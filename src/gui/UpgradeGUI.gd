extends Control

const UpgradePanel = preload("res://src/gui/UpgradePanel.tscn")

@onready var upgrade_grid: GridContainer = %UpgradeGrid
@onready var done_button: Button = %DoneButton

@onready var upgrade_definitions: Upgrades = preload("res://src/scenes/upgrades.tres")

func _ready() -> void:
	EventBus.wave_end.connect(show_upgrade_gui)
	EventBus.update_upgrades.connect(populate_upgrade_grid)
	done_button.button_up.connect(done)

	populate_upgrade_grid()

func done() -> void:
	visible = false
	EventBus.wave_start.emit()

func show_upgrade_gui() -> void:
	visible = true

func populate_upgrade_grid() -> void:
	for child in upgrade_grid.get_children():
		child.queue_free()
	
	for upgrade_definition in upgrade_definitions.upgrades:
		print(upgrade_definition.name)
		var grid_panel: UpgradePanel = UpgradePanel.instantiate()
		
		grid_panel.gui_input.connect(func(event: InputEvent):
			if event.is_action_pressed("fire-primary"):
				EventBus.upgrade_clicked.emit(upgrade_definition)
		)
		
		upgrade_grid.add_child(grid_panel)
		grid_panel.set_upgrade_definition(upgrade_definition)
