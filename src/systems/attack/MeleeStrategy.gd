class_name MeleeStrategy
extends AttackStrategy

@onready var attack_timer: Timer = $AttackTimer

func start(_target: Node2D):
	pass

func attack():
	if !target:
		return
		
	super()
	target.get_node("Health").emit_signal("damaged", ATTACK_DAMAGE)
	attack_timer.start(ATTACK_SPEED)

func _on_area_entered(area):
	if area.get_parent() && area.get_parent().is_in_group("player"):
		target = area.get_parent()
		attack()

func _on_area_exited(area):
	if area.get_parent() && area.get_parent().is_in_group("player"):
		attack_timer.stop()
		target = null

func _on_attack_timer_timeout():
	if target:
		attack()
