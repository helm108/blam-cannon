class_name RangedStrategy
extends AttackStrategy

signal set_attack_range(attack_range: float)
signal firing(is_firing: bool)

const bulletScene = preload("res://src/scenes/bullet/bullet.tscn")

@onready var root: Node2D = get_tree().current_scene
@onready var attack_timer: Timer = $AttackTimer
@onready var aim_timer: Timer = $AimTimer
@onready var recover_timer: Timer = $RecoverTimer

var parent: Enemy

func _ready() -> void:
	parent = get_parent()

func start(_target: Node2D):
	target = _target
	
	set_attack_range.emit(ATTACK_RANGE)
	
	attack_timer.timeout.connect(attack)
	aim_timer.timeout.connect(_on_aimed)
	recover_timer.timeout.connect(_on_recovered)
	
	attack_timer.start(ATTACK_SPEED)
	
func attack():
	if !target:
		return
	
	if parent.position.distance_to(target.position) > ATTACK_RANGE:
		return
		
	super()
	
	# tell entity to stop moving
	firing.emit(true)
	aim_timer.start(AIM_TIME)

func _fire() -> void:	
	var bullet: Bullet = bulletScene.instantiate()
	bullet.fire(root, parent.rotation, parent, BULLET_SPEED, ATTACK_DAMAGE, "player")

func _on_aimed() -> void:
	_fire()
	recover_timer.start(RECOVER_TIME)
	pass

func _on_recovered():
	# tell entity to start moving
	firing.emit(false)
	pass	
