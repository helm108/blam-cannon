class_name MovementStrategy
extends Node

@export var MOVE_SPEED: float = 50.0

var ATTACK_RANGE: float

var target: Node2D

var should_move: bool = true

func move(_target: Node2D) -> void:
	self.target = _target

func start() -> void:
	should_move = true

func stop() -> void:
	should_move = false

func set_range(_range: float) -> void:
	ATTACK_RANGE = _range
