class_name CircleStrategy
extends MovementStrategy

var clockwise: bool

var parent: CharacterBody2D

func _ready() -> void:
	parent = get_parent()
	clockwise = randf() > 0.5

func move(_target: Node2D) -> void:
	super(_target)

func _approach(delta: float) -> void:
	parent.look_at(target.position)
	
	var angle_to_target: Vector2 = parent.position.direction_to(target.position)
	
	var move_vector: Vector2 = angle_to_target * MOVE_SPEED * delta
	parent.move_and_collide(move_vector)

func _circle(delta: float) -> void:
	parent.look_at(target.position)
	
	var angle_to_target: Vector2 = parent.position.direction_to(target.position)
	
	if clockwise:
		angle_to_target = Vector2(-angle_to_target.y, angle_to_target.x)
	else:
		angle_to_target = Vector2(angle_to_target.y, -angle_to_target.x)
	
	var move_vector: Vector2 = angle_to_target * MOVE_SPEED * delta
	parent.move_and_collide(move_vector)

func _process(delta: float):
	if !target || !parent:
		return
	
	if !should_move:
		return
	
	var distance_to_target = parent.position.distance_to(target.position)
	
	if distance_to_target > ATTACK_RANGE:
		_approach(delta)
	else:
		_circle(delta)
