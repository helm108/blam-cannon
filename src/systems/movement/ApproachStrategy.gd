class_name ApproachStrategy
extends MovementStrategy

var parent: CharacterBody2D

func _ready() -> void:
	parent = get_parent()
	
func move(_target: Node2D) -> void:
	super(_target)

func _process(delta):
	if !target:
		return
	
	if !should_move:
		return
	
	parent.look_at(target.position)
	var angle_to_target: Vector2 = parent.position.direction_to(target.position)
	
	var move_vector: Vector2 = angle_to_target * MOVE_SPEED * delta
	parent.move_and_collide(move_vector)
