class_name WaveSpawner
extends Node

signal wave_started(wave_number: int)

@onready var root: Node2D = get_tree().current_scene
@onready var turret: CharacterBody2D = %turret

@export var SPAWN_DISTANCE: int = 750
@export var GROUP_SPAWN_TIME: int = 5
@export var wave_number: int = 1
@export var group_spawn_wait: float = 5

const WAVE_CLEARED_DURATION = 2

var wave_data: WavesData = preload("res://src/scenes/waves.tres")

var wave_count: int

var enemies: Array[Enemy] = []

func _ready() -> void:
	wave_count = wave_data.waves.size()
	
	EventBus.wave_start.connect(spawn)

func _spawn_entity(resource: PackedScene) -> void:
	var direction: Vector2 = Vector2(randf_range(-1, 1), randf_range(-1, 1)).normalized()

	var enemy: Enemy = resource.instantiate()
	enemy.set_target(turret)
	enemy.position = turret.position + (direction * SPAWN_DISTANCE)

	root.call_deferred("add_child", enemy)
	
	enemies.append(enemy)
	var enemy_health: Health = enemy.get_node("Health")
	enemy_health.dead.connect(_entity_destroyed)

func _entity_destroyed(entity: Enemy):
	enemies.erase(entity)
	
	if enemies.is_empty():
		_wave_complete()

func _wave_complete():
	EventBus.show_message.emit("Wave %s Cleared!" % wave_number, WAVE_CLEARED_DURATION)
	await get_tree().create_timer(WAVE_CLEARED_DURATION).timeout
	wave_number += 1
	EventBus.wave_end.emit()

func spawn() -> void:
	if wave_number > wave_count:
		EventBus.show_message.emit("MISSION ACCOMPLISHED", 10)
		return
		
	wave_started.emit(wave_number)
	
	var wave = wave_data.waves[wave_number - 1]
	
	var messages: Array[String] = [
		"Wave %s In 3" % wave_number,
		"Wave %s In 2" % wave_number,
		"Wave %s In 1" % wave_number,
	]
	var message_duration: int = 1
	EventBus.queue_messages.emit(messages, message_duration)
	await get_tree().create_timer(messages.size() * message_duration).timeout

	for _groups in wave.groups:
		var groups: GroupsData = _groups
		for enemy in groups.enemies:
			for i in enemy.count:
				_spawn_entity(enemy.enemy)
			# TODO do this with a signal
			await get_tree().create_timer(GROUP_SPAWN_TIME).timeout
