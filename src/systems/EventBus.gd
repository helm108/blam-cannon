extends Node

signal show_message(value: String, duration: int)
signal queue_messages(values: Array[String], duration: int)
signal clear_message

signal wave_start
signal wave_end

signal upgrade_clicked(upgrade_definition: UpgradeDefinition)
signal update_upgrades
