extends Node

signal money_updated(value: int)
signal health_updated(current: int, max: int)

var HEALTH: int = 100
var MAX_HEALTH: int = 100;

var MONEY: int = 10000

var BULLET_FIRE_RATE: float = 0.5
var BULLET_PENETRATION: int = 1
var BULLET_DAMAGE: int = 5
var BULLET_SPEED: int = 1000
var ROTATION_RATE: float = 1.0

var LASER_POINTER: bool = false

func _ready() -> void:
	money_updated.emit(MONEY)
	health_updated.emit(HEALTH, MAX_HEALTH)

func update_balance(value: int) -> void:
	MONEY += value
	MONEY = max(0, MONEY)
	
	money_updated.emit(MONEY)

func set_health(value: int) -> void:
	HEALTH = value
	health_updated.emit(HEALTH, MAX_HEALTH)

func update_max_health(value: int) -> void:
	MAX_HEALTH += value
	MAX_HEALTH = max(0, MAX_HEALTH)
	
	if HEALTH > MAX_HEALTH:
		HEALTH = MAX_HEALTH
	
	health_updated.emit(HEALTH, MAX_HEALTH)

func enable_laser_pointer() -> void:
	LASER_POINTER = true
