class_name AttackStrategy
extends Node

@export var ATTACK_RANGE: float = 0
@export var ATTACK_DAMAGE: float = 0
@export var ATTACK_SPEED: float = 0
@export var BULLET_SPEED: float = 0
@export var AIM_TIME: float = 0
@export var RECOVER_TIME: float = 0

var target: Node2D;

func start(_target: Node2D):
	target = _target

func attack():
	pass
