class_name Enemy
extends CharacterBody2D

var CreepDeath = preload("res://src/effects/CreepDeath.tscn")

@onready var root: Node2D = get_tree().current_scene
@onready var movement_strategy: MovementStrategy = $MovementStrategy
@onready var attack_strategy: AttackStrategy = $AttackStrategy
@onready var sprite_2d: Sprite2D = $Sprite2D

@export var money_value: int = 10

var target: Node2D

func set_target(_target: Node2D):
	self.target = _target

func _ready():
	movement_strategy.move(target)
	attack_strategy.start(target)

func _on_health_dead(_entity: Enemy):
	PlayerState.update_balance(money_value)
	var creep_death: Node2D = CreepDeath.instantiate()
	creep_death.global_position = global_position
	root.add_child(creep_death)

func _on_attack_strategy_firing(is_firing):
	if is_firing:
		movement_strategy.stop()
	else:
		movement_strategy.start()

func _on_attack_strategy_set_attack_range(attack_range: float) -> void:
	movement_strategy.set_range(attack_range)
