extends Resource
class_name UpgradeDefinition

@export var name: String = ''
@export_multiline var description: String = ''
@export var cost: int
@export var property: String = ''
@export var upgrade_value: float
@export var level: int = 0
@export var max_level: int = 1
@export var cost_multiplier: int = 1
@export var texture: Texture

func get_cost() -> int:
	return cost if level == 0 else cost * (cost_multiplier * level)

func is_purchased() -> bool:
	return level == max_level

func purchased() -> void:
	level += 1
