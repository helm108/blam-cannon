extends Node

@onready var turret: CharacterBody2D = %turret

func _ready() -> void:
	EventBus.upgrade_clicked.connect(purchase_upgrade)

func purchase_upgrade(upgrade_definition: UpgradeDefinition) -> void:
	var upgrade_cost: int = upgrade_definition.get_cost()

	if upgrade_definition.is_purchased():
		return

	if upgrade_cost <= PlayerState.MONEY:
		PlayerState.update_balance(-upgrade_cost)
		# Update GUI
		upgrade_definition.purchased()
		
		if upgrade_definition.name == 'Laser Pointer':
			PlayerState.enable_laser_pointer()
			var laser_pointer: RayCast2D = turret.get_node("tower/LaserPointer")
			laser_pointer.visible = true
		else:
			PlayerState[upgrade_definition.property] += upgrade_definition.upgrade_value
		
		EventBus.update_upgrades.emit()
