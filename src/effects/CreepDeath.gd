extends Node2D
@onready var gpu_particles_2d: GPUParticles2D = $GPUParticles2D
@onready var timer: Timer = $Timer

@export var lifetime = 1

func _ready() -> void:
	gpu_particles_2d.lifetime = lifetime
	gpu_particles_2d.emitting = true
	timer.start(lifetime)

func _on_timer_timeout() -> void:
	queue_free()
