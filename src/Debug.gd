extends CanvasLayer

@onready var timer_label: Label = $Control/VFlowContainer/TimerLabel
@onready var fps_label: Label = $Control/VFlowContainer/FPSLabel
@onready var timer: Timer = $Timer

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	fps_label.set_text("FPS: %d" % Engine.get_frames_per_second())
	
	var time = Time.get_ticks_msec() / 1000.0
	timer_label.set_text("%ds elapsed" % time)
