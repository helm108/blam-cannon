class_name Bullet
extends Node2D

@onready var gpu_particles_2d: GPUParticles2D = $GPUParticles2D
@onready var timer: Timer = $Timer
@export var LIFETIME: float = 5.0

var penetration_count: int = 0;

var damage: float = PlayerState.BULLET_DAMAGE
var bullet_speed: float
var bullet_damage: float
var target_group: String

func _process(delta: float) -> void:
	var direction = Vector2.RIGHT.rotated(self.rotation).normalized() * bullet_speed * delta
	self.position += direction

func _on_timer_timeout() -> void:
	self.queue_free()
	
func fire(root: Node2D, _rotation: float, _spawn_position: Node2D, _bullet_speed: float, _bullet_damage: float, _target_group: String) -> void:
	root.add_child(self)
	global_position = _spawn_position.global_position
	rotation = _rotation
	penetration_count = PlayerState.BULLET_PENETRATION
	bullet_speed = _bullet_speed
	bullet_damage = _bullet_damage
	target_group = _target_group
	
	timer.start(LIFETIME)

func _on_area_2d_area_entered(area: Area2D):
	if area.get_parent().is_in_group(target_group) && penetration_count > 0:
		area.get_parent().get_node("Health").emit_signal("damaged", bullet_damage)
		penetration_count -= 1
		if penetration_count == 0:
			queue_free()
