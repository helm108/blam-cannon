extends RayCast2D

@onready var line_2d: Line2D = $Line2D

var max_length: int = 750
var line_thickness: int = 8
var length := Vector2(max_length, 0)

func _ready() -> void:
	line_2d.add_point(position)
	line_2d.add_point(Vector2(max_length, 0))

func _process(_delta: float) -> void:
	if is_colliding():
		var distance_to_target = global_position.distance_to(get_collision_point())
		length.x = distance_to_target
	else:
		length.x = max_length

	line_2d.set_point_position(1, length)

	var ratio: float = line_thickness / length.x * 100
	line_2d.material.set_shader_parameter("ratio", ratio)
