class_name Health
extends Area2D

signal damaged
signal dead(entity: Enemy)

@onready var parent = get_parent()

@onready var panel_container = $PanelContainer
@onready var progress_bar: ProgressBar = $PanelContainer/ProgressBar

@export var health: int = 10
@export var max_health: int = 10

func _ready():
	progress_bar.max_value = max_health
	progress_bar.value = health
	
	damaged.connect(_on_damaged)

func _process(_delta):
	if "rotation" in parent:
		panel_container.rotation = -parent.rotation

func _on_damaged(value: int):
	health -= value
	health = clamp(health, 0, max_health)

	progress_bar.value = health

	if parent.is_in_group("player"):
		PlayerState.set_health(health)

	if health == 0:
		dead.emit(parent)
		parent.queue_free()
