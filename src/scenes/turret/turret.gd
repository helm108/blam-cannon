extends Node2D

@onready var root: Node2D = get_tree().current_scene
@onready var tower: Sprite2D = $tower
@onready var muzzle: Node2D = $tower/barrel/muzzle
@onready var bullet_fire_rate_timer: Timer = $BulletFireRateTimer
@onready var turret_recoil: AnimationPlayer = $TurretRecoil
@onready var muzzle_smoke: GPUParticles2D = $tower/barrel/muzzle/muzzle_smoke

const bulletScene = preload("res://src/scenes/bullet/bullet.tscn")

func _rotate(delta: float) -> void:
	var mouse_direction: Vector2 = tower.position.direction_to(get_local_mouse_position()).normalized()
	var turret_direction: Vector2 = Vector2.RIGHT.rotated(tower.rotation).normalized()

	# Angle between mouse vector and turret vector.
	var angle_to_mouse: float = mouse_direction.angle_to(turret_direction)

	# Set the rotation rate and direction based on the above angle.
	# Clockwise is positive, anti-clockwise is negative.
	var rotation_rate: float = (PlayerState.ROTATION_RATE if angle_to_mouse < 0 else -PlayerState.ROTATION_RATE) * delta

	# If the angle to the mouse is smaller than a single rotation step:
	if abs(angle_to_mouse) < abs(rotation_rate):
		# Set the rotation rate to the remaining distance.
		rotation_rate = -angle_to_mouse

	# Rotate the tower.
	tower.rotate(rotation_rate)

func _fire_primary():
	if bullet_fire_rate_timer.is_stopped():
		var bullet: Bullet = bulletScene.instantiate()
		bullet.fire(root, tower.rotation, muzzle, PlayerState.BULLET_SPEED, PlayerState.BULLET_DAMAGE, "enemy")
		bullet_fire_rate_timer.start(PlayerState.BULLET_FIRE_RATE)
		turret_recoil.play("TowerRecoil")
		muzzle_smoke.restart()
		muzzle_smoke.emitting = true

func _process(delta: float) -> void:
	_rotate(delta)

func _input(event):
	if event.is_action_pressed("fire-primary"):
		_fire_primary()
