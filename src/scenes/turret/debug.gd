extends Node2D

@onready var label: Label = $Label
@onready var tower: Sprite2D = $"../tower"
@onready var mouse_vector: Line2D = $mouseVector
@onready var turret_vector: Line2D = $turretVector
@onready var rich_text_label: RichTextLabel = $RichTextLabel

func draw_lines(mouseVector: Vector2, turretVector: Vector2):
	mouse_vector.clear_points()
	mouse_vector.add_point(tower.position)
	mouse_vector.add_point(mouseVector * 100)

	turret_vector.clear_points()
	turret_vector.add_point(tower.position)
	turret_vector.add_point(turretVector * 100)

func _process(_delta: float) -> void:
	var mouseVector: Vector2 = tower.position.direction_to(get_local_mouse_position()).normalized()
	var turretVector: Vector2 = Vector2.RIGHT.rotated(tower.rotation).normalized()

	draw_lines(mouseVector, turretVector)
