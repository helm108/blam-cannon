# Blam Cannon

## To Do
-[x] Wave System
-[ ] Reloading timer
-[ ] Special Weapon
-[ ] Upgrades
-[ ] Fuzzy circle representing visual range of 500px?
-[ ] Cloud shadow shader

## Upgrades
When a wave is completed, an upgrade menu will appear.
Upgrades cost money which is earned by killing creeps.
Some upgrades have prerequisites, e.g. extra barrel upgrades.

- laser sight (line showing turret direction)
- extra barrel (can be purchased multiple times, bullets fire simultaneously)
- rate of fire
- turning speed
- single-shot vs ripple-shot for multi-barrel turret

## Upgrade System
`gui/UpgradeGui.` iterates through the `UpgradeDefinition` collection and creates an `UpgradePanel` for each one.

UpgradeGui listens for clicks on each panel, and when a Panel is clicked UpgradeGui emits the signal `EventBus.upgrade_clicked` with the Definition as its payload.

`UpgradeService.gd` then listens for this event and processes the purchase if the player has enough money.

- The player's balance is updated
- The definition is informed that it has been purchased, which modifies some properties of the definition
- `EventBus.update_upgrades` is emitted to indicate that an upgrade has been modified

 `UpgradeGui.gd` listens for `update_upgrades` and re-renders the Upgrade UI

## Wave System

signal WAVE_READY
three second timer on screen
signal to spawn the first group in the current wave
	start a timer that spawns the next group
when all spawned entities are destroyed, increment wave count
signal WAVE_READY

1. Countdown until a wave will be spawned
2. Text when a wave is destroyed
3. Pause and bring up the upgrade menu
4. Countdown until the next wave

Do waves have sub-waves? If I put in 10 Melee do they all spawn at once, trickle in, arrive in groups?
5 creeps will take 10 seconds to kill assuming every shot is perfect (1s fire rate, 2 hits per creep)
